package com.example.viewpager.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.example.viewpager.R


class FragmentOne: Fragment(R.layout.fragment_1) {

    private lateinit var textURL: EditText
    private lateinit var btnNext: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textURL = view.findViewById(R.id.text_URL)
        btnNext = view.findViewById(R.id.btn_next)
        btnNext.setOnClickListener {

            val URL = textURL.text.toString()
            if (textURL.text.isEmpty()) {
                return@setOnClickListener
            }

            FragmentOneDirections.actionMenuFragmentToCatFragment()
            controller.navigate(action)

        }
    }


}
