package com.example.viewpager.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.viewpager.R

class FragmentThree: Fragment(R.layout.fragment_3) {
    private lateinit var textNote: TextView
    private lateinit var writeNote: EditText
    private lateinit var buttonAdd: Button
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        textNote = view.findViewById(R.id.textNote)
        writeNote = view.findViewById(R.id.writeNote)
        buttonAdd = view.findViewById(R.id.buttonAdd)
        buttonAdd.setOnClickListener {
            val note = writeNote.text.toString()
            val text = textNote.text.toString()
            val result = note + "\n" + text
            if (writeNote.text.isEmpty()) {
                return@setOnClickListener
            }
            textNote.text = result
            writeNote.text.clear()
        }
    }


}